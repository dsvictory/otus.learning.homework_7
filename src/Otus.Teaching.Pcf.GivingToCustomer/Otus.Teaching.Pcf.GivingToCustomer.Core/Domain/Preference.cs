﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Preference
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}