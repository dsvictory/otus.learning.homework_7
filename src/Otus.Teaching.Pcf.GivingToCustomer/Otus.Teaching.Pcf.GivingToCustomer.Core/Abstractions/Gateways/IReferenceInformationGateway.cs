﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways {
    public interface IReferenceInformationGateway {

        Task<Preference> GetPreferenceAsync(Guid id);

        Task<IEnumerable<Preference>> GetPreferencesAsync();

        Task<IEnumerable<Preference>> GetRangePreferencesAsync(IEnumerable<Guid> ids);

    }
}
