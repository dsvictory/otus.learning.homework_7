﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration {
    public class ReferenceInformationGateway : IReferenceInformationGateway {
        private const string MAIN_ADDRESS_PART = "api/v1/preferences";

        private readonly HttpClient _httpClient;

        public ReferenceInformationGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GetPreferenceAsync(Guid id) {

            var response = await _httpClient.GetAsync(@$"{MAIN_ADDRESS_PART}/id/{id}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsAsync<Preference>(new[] { new JsonMediaTypeFormatter() });
            }
            else {
                throw new HttpRequestException();
            }
        }

        public async Task<IEnumerable<Preference>> GetPreferencesAsync() {
            var response = await _httpClient.GetAsync(MAIN_ADDRESS_PART);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsAsync<IEnumerable<Preference>>(new[] { new JsonMediaTypeFormatter() });
            }
            else {
                throw new HttpRequestException();
            }
        }

        public async Task<IEnumerable<Preference>> GetRangePreferencesAsync(IEnumerable<Guid> ids) {
            var response = await _httpClient.GetAsync(@$"{MAIN_ADDRESS_PART}/ids/ids={string.Join("&ids=", ids)}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsAsync<IEnumerable<Preference>>(new[] { new JsonMediaTypeFormatter() });
            }
            else {
                throw new HttpRequestException();
            }
        }
    }
}