﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReferenceInformation.Core.Domain {
    public class BaseEntity {

        public Guid Id { get; set; }
    }
}
