﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;

namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess
{
    public class DataContext
        : DbContext
    {

        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}