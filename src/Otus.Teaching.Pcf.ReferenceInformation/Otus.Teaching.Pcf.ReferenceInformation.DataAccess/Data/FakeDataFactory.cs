﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;

namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };
    }
}