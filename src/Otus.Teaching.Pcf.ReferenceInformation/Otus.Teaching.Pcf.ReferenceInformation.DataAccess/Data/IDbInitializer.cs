﻿namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}