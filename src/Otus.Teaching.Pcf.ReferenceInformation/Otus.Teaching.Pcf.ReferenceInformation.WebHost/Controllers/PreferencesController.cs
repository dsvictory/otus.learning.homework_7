﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;
using Otus.Teaching.Pcf.ReferenceInformation.WebHost.Models;

namespace Otus.Teaching.Pcf.ReferenceInformation.WebHost.Controllers {
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        const string ALL_PREFERENCES_KEY = "AllPreferences";

        private readonly IRepository<Preference> _preferencesRepository;

        private readonly IDistributedCache _cache;

        private readonly ILogger<PreferencesController> _logger;

        public PreferencesController(IRepository<Preference> preferencesRepository, 
            IDistributedCache cache, 
            ILogger<PreferencesController> logger)
        {
            _preferencesRepository = preferencesRepository;
            _cache = cache;
            _logger = logger;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            string cached = await _cache.GetStringAsync(ALL_PREFERENCES_KEY);
            if (cached != null) {
                _logger.LogInformation("All preferences from redis");
                return Ok(JsonSerializer.Deserialize<List<PreferenceResponse>>(cached));
            }

            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            await _cache.SetStringAsync(ALL_PREFERENCES_KEY, JsonSerializer.Serialize(response),
                new DistributedCacheEntryOptions() {
                    AbsoluteExpirationRelativeToNow = new TimeSpan(0, 0, 0, 15)
                });

            return Ok(response);
        }

        /// <summary>
        /// Получить предпочтение по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id) 
        {
            string cacheKey = id.ToString();

            string cached = await _cache.GetStringAsync(cacheKey);
            if (cached != null) {
                _logger.LogInformation("Preference from redis");
                return Ok(JsonSerializer.Deserialize<PreferenceResponse>(cached));
            }

            var response = await _preferencesRepository.GetByIdAsync(id);

            await _cache.SetStringAsync(cacheKey, JsonSerializer.Serialize(response),
                new DistributedCacheEntryOptions() {
                    AbsoluteExpirationRelativeToNow = new TimeSpan(0, 0, 0, 15)
                });

            return Ok(response);
        }

        /// <summary>
        /// Получить предпочтения по нескольким id
        /// </summary>
        /// <returns></returns>
        [HttpGet("ids/{ids}")]
        public async Task<ActionResult<List<Preference>>> GetRangePreferencesAsync([FromQuery] List<Guid> ids) 
        {
            string cacheKey = string.Join(",", ids);

            string cached = await _cache.GetStringAsync(cacheKey);
            if (cached != null) {
                _logger.LogInformation("Preferences by their ids rom redis");
                return Ok(JsonSerializer.Deserialize<PreferenceResponse>(cached));
            }

            var response = await _preferencesRepository.GetRangeByIdsAsync(ids);

            await _cache.SetStringAsync(cacheKey, JsonSerializer.Serialize(response),
                new DistributedCacheEntryOptions() {
                    AbsoluteExpirationRelativeToNow = new TimeSpan(0, 0, 0, 15)
                });

            return Ok(response);
        }




        [HttpPost]
        public int Clear() {
            _cache.Remove(ALL_PREFERENCES_KEY);
            return 1;
        }
    }
}