﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration {
    public class ReferenceInformationGateway : IReferenceInformationGateway {

        private readonly HttpClient _httpClient;

        public ReferenceInformationGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Preference> GetPreferenceAsync(Guid id) {

            var response = await _httpClient.GetAsync($"api/v1/preferences/id/{id}");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await response.Content.ReadAsAsync<Preference>(new[] { new JsonMediaTypeFormatter() });
            }
            else {
                throw new HttpRequestException();
            }
        }
    }
}
