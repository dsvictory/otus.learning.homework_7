﻿using System;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain
{
    public class Preference
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}